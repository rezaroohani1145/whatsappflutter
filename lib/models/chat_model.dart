class ChatModel {
  final String name;
  final String message;
  final String time;
  final String avatarUrl;

  ChatModel({required this.name, required this.message, required this.time, required this.avatarUrl});
}

List<ChatModel> dummyData = [
  new ChatModel(
      name: "شکیلا جان",
      message: "سلام چطوری, دوره فلاتر چطوره ؟",
      time: "15:30",
      avatarUrl:
          "https://static.roocket.ir/public/images/useravatar/2020/5/31/1590877814.png"),
  new ChatModel(
      name: "محمد سلیمانی",
      message: "سلام, امروز چیکاره ایم ؟",
      time: "17:30",
      avatarUrl:
          "https://static.roocket.ir/images/avatar/2021/12/22/w5c1X1KSJjw54PCC8iPPTtIuPwiwTYgBEVY2sRYX.png"),
  new ChatModel(
      name: "محمد احمدی",
      message: "به به چه خبر",
      time: "5:00",
      avatarUrl:
          "https://static.roocket.ir/images/avatar/2021/12/22/w5c1X1KSJjw54PCC8iPPTtIuPwiwTYgBEVY2sRYX.png"),
  new ChatModel(
      name: "فاطمه محمدیان",
      message: "سلام دوره فلاتر کی به اتمام میرسه ؟",
      time: "10:30",
      avatarUrl:
          "https://static.roocket.ir/images/avatar/2021/12/22/w5c1X1KSJjw54PCC8iPPTtIuPwiwTYgBEVY2sRYX.png"),
  new ChatModel(
      name: "حسین بابای",
      message: "سلام آقا راکت اصلا بدرد نمیخوره :))",
      time: "12:30",
      avatarUrl:
          "https://static.roocket.ir/images/avatar/2021/12/22/w5c1X1KSJjw54PCC8iPPTtIuPwiwTYgBEVY2sRYX.png"),
  new ChatModel(
      name: "اکبر",
      message: "سلام کی تخفیف میزارید",
      time: "15:30",
      avatarUrl:
          "https://static.roocket.ir/images/avatar/2021/12/22/w5c1X1KSJjw54PCC8iPPTtIuPwiwTYgBEVY2sRYX.png"),
];
