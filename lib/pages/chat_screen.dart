import 'package:flutter/material.dart';
import 'package:whatsapp/models/chat_model.dart';
class ChatScreen extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return ListView.builder(
        itemCount:dummyData.length ,
        itemBuilder: (context , index){
          return Column(
            children: [
              Divider(
                height: 10,
              ),
              ListTile(
                leading: CircleAvatar(
                  backgroundImage: NetworkImage(dummyData[index].avatarUrl),
                  backgroundColor: Colors.grey,
                ),
                title: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      dummyData[index].name,
                      style: TextStyle(fontWeight:FontWeight.bold ),
                    ),
                    Text(
                      dummyData[index].time,
                      style: TextStyle(color: Colors.grey,fontSize: 14),
                    )
                  ],
                ),
                subtitle: Container(
                  padding: const EdgeInsets.only(top: 5),

                  child: Text(
                    dummyData[index].message,
                    style: TextStyle(color: Colors.grey,fontSize: 12),
                  ),
                ),
              )
            ],
          );
        }
    );
  }

}