import 'package:flutter/material.dart';
import 'package:whatsapp/pages/call_screen.dart';
import 'package:whatsapp/pages/chat_screen.dart';
import 'package:whatsapp/pages/status_screen.dart';
import 'package:whatsapp/pages/camera_screen.dart';
class whatsAppHome extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => WhatsAppHomeState();
  }


class WhatsAppHomeState extends State<whatsAppHome>with SingleTickerProviderStateMixin{
  late TabController tabController;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    tabController = TabController(initialIndex: 1,length: 4, vsync: this);
  }
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text('واتساپ'),
        elevation: 5,
        bottom: TabBar(
          controller: tabController,
          indicatorColor: Colors.white,
          tabs: [
            Tab(icon: Icon(Icons.camera_alt),),
            Tab(text: 'گفتگو ها',icon: Icon(Icons.local_convenience_store)),
            Tab(text: 'وضعیت ها',icon: Icon(Icons.amp_stories)),
            Tab(text: 'تماس ها',icon: Icon(Icons.call),),
          ],
        ),
        actions: [
          Icon(Icons.search),
          Padding(padding: EdgeInsets.symmetric(horizontal: 5)),
          PopupMenuButton<String>(
              onSelected: (String choice){print(choice);},
              itemBuilder: (BuildContext Context){
               return[
                 PopupMenuItem(
                   child: Row(
                     mainAxisAlignment: MainAxisAlignment.end,
                     children: [
                       Text('کاربران',style: TextStyle(fontSize: 14,fontWeight: FontWeight.bold),)
                     ],
                   ),
                 ),
                 PopupMenuItem(
                   child: Row(
                     mainAxisAlignment: MainAxisAlignment.end,
                     children: [
                       Text('تنظیمات',style: TextStyle(fontSize: 14,fontWeight: FontWeight.bold),)
                     ],
                   )
                 )
            
               ] ;
              }
          ),
        ],
      ),
      body: TabBarView(
        controller: tabController,
        children: [
          CameraScreen(),
          ChatScreen(),
          StatusScreen(),
          CallScreen(),
        ],

      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {print('open chat');},
        child: Icon(Icons.message,color: Colors.white,),
      )
    );
  }
  
}